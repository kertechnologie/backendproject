
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="opportunite")
public class Opportunite implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idopportunite;
	
	@Column(name = "client")
	private String client;
	
	@Column(name = "interlocuteur")
	private String interlocuteur;
	
	@Column(name = "tel")
	private String tel;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "commentaire")
	private String commentaire;
	
	@Column(name = "commerciale")
	private String commerciale;
	
	@Column(name = "app_affaire")
	private String apporteur_affaire;
	
	
	public Opportunite(int idopportunite, String client, String interlocuteur,String tel, String email,
			String commentaire, String commerciale, String apporteur_affaire) {
		super();
		this.idopportunite = idopportunite;
		this.client = client;	
		this.interlocuteur = interlocuteur;
		this.tel = tel;
		this.email = email;
		this.commentaire = commentaire;
		this.commerciale = commerciale;
		this.apporteur_affaire = apporteur_affaire;
	}

	public Opportunite() {
		super();
	}

	public int getIdopportunite() {
		return idopportunite;
	}

	public void setIdopportunite(int idopportunite) {
		this.idopportunite = idopportunite;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
	
	public String getInterlocuteur() {
		return interlocuteur;
	}

	public void setInterlocuteur(String interlocuteur) {
		this.interlocuteur = interlocuteur;
	}


	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCommentaire() { return commentaire; }

	public void setCommentaire(String commentaire) { this.commentaire = commentaire; }
	
	public String getCommerciale() { return commerciale; }

	public void setCommerciale(String commerciale) { this.commerciale = commerciale; }
	
	public String getApporteur_affaire() { return apporteur_affaire; }

	public void setApporteur_affaire(String apporteur_affaire) { this.apporteur_affaire = apporteur_affaire; }

	@Override
	public String toString() {
		return "Client [idopportunite=" + idopportunite + ", client=" + client + ", interlocuteur=" + interlocuteur
				+ ", tel=" + tel + ","
				+ " email=" + email + ", commentaire=" + commentaire + ", commerciale=" + commerciale + ","
				+ " apporteur_affaire=" + apporteur_affaire + "]";
	}


}
