package com.javainuse.model;

import java.io.Console;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="affectation")
public class Affectation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    int idaffectation;
 
    @ManyToOne(fetch = FetchType.EAGER)   
    @JoinColumn(name = "idemp")
    @OnDelete(action = OnDeleteAction.CASCADE) 
    User user;
 
    @ManyToOne(fetch = FetchType.EAGER)  
    @JoinColumn(name = "idprojet")   
    Projet projet;

    @OneToMany(mappedBy = "affectation",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonProperty(access = Access.WRITE_ONLY)
    List<Tache> taches;
    


	public Affectation(int idaffectation, User user, Projet projet,List<Tache> taches) {
		this.idaffectation = idaffectation;
		this.user = user;
		this.projet = projet;
		this.taches = taches;
	}
	
	public Affectation() {
		
	}

	public int getIdaffectation() {
		return idaffectation;
	}

	public void setIdaffectation(int idaffectation) {
		this.idaffectation = idaffectation;
	}
	@JsonProperty(access = Access.WRITE_ONLY)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public List<Tache> getTaches() {
		return taches;
	}

	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}

	@PreRemove
	private void removeAffectationFromUser() {
		//System.out.println("preremove Affectation");
		if(user != null) {
			
			user.getAffectations().remove(this);
			this.user = null ;
			this.projet = null ;
			this.getTaches().clear();
			
			
		}

	}
   
	
	@Override
	public String toString() {
		return "Affectation [idaffectation=" + idaffectation + ", user=" + user.getIdemp() + ", projet=" + projet.getIdprojet() + ", taches="
				+ taches + "]";
	}
	
	
}
