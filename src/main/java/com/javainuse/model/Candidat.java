
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="candidat")
public class Candidat implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idcandidat;
	
	@Column(name = "specialite")
	private String specialite;
	
	@Column(name = "nom")
	private String nom;
		
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "poste")
	private String poste;
	
	@Column(name = "tel")
	private String tel;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "competence")
	private String competence;
	
	@Column(name = "statut")
	private String statut;
	
	@Column(name = "commentaire")
	private String commentaire;
	
	public Candidat(int idcandidat, String specialite, String nom,String prenom,String poste,String tel, String email,
			String competence, String statut, String commentaire) {
		super();
		this.idcandidat = idcandidat;
		this.specialite = specialite;	
		this.nom = nom;
		this.prenom = prenom;
		this.poste = poste;
		this.tel = tel;
		this.email = email;
		this.competence = competence;
		this.statut = statut;
		this.commentaire = commentaire;
	}

	public Candidat() {
		super();
	}

	public int getIdcandidat() {
		return idcandidat;
	}

	public void setIdcandidat(int idcandidat) {
		this.idcandidat = idcandidat;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCompetence() { return competence; }

	public void setCompetence(String competence) { this.competence = competence; }
	
	public String getStatut() { return statut; }

	public void setStatut(String statut) { this.statut = statut; }
	
	public String getCommentaire() { return commentaire; }

	public void setCommentaire(String commentaire) { this.commentaire = commentaire; }

	@Override
	public String toString() {
		return "Candidat [idcandidat=" + idcandidat + ", societe=" + specialite + ", nom_interlocuteur=" + nom
				+ ", prenom=" + prenom + ", poste=" + poste + ", tel=" + tel + ", email=" + email + ", adresse_postale_societe="
				+ competence + ", statut=" + statut + ", commentaire=" + commentaire + "]";
	}






}
