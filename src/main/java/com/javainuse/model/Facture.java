
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="facture")
public class Facture implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idfacture;
	
	@Column(name = "client")
	private String client;
	
	@Column(name = "num_facture")
	private String num_facture;
		
	@Column(name = "date_facture")
	private String date_facture;
	
	@Column(name = "message")
	private String message;
	
	@Column(name = "num_engagement")
	private String num_engagement;
	
	@Column(name = "code_service")
	private String code_service;
	
	@Column(name = "condition_paiement")
	private String condition_paiement;
	
	@Column(name = "echeance")
	private String echeance;
	
	@Column(name = "consultant")
	private String consultant;
	
	@Column(name = "annee")
	private String annee;
	
	@Column(name = "mois")
	private String mois;
	
	@Column(name = "nbr_jours")
	private String nbr_jours;
	
	@Column(name = "unite")
	private String unite;
	
	@Column(name = "tjm")
	private String tjm;
	
	@Column(name = "taux_tva")
	private String taux_tva;
	
	@Column(name = "montant")
	private String montant;
	
	public Facture(int idfacture, String client, String num_facture,String date_facture,String message,String num_engagement,
			String code_service, String condition_paiement, String echeance, String consultant, String annee,
			String mois, String nbr_jours, String unite, String tjm, String taux_tva, String montant) {
		super();
		this.idfacture = idfacture;
		this.client = client;	
		this.num_facture = num_facture;
		this.date_facture = date_facture;
		this.message = message;
		this.num_engagement = num_engagement;
		this.code_service = code_service;
		this.condition_paiement = condition_paiement;
		this.echeance = echeance;
		this.consultant = consultant;
		this.annee = annee;
		this.mois = mois;
		this.nbr_jours = nbr_jours;
		this.unite = unite;
		this.tjm = tjm;
		this.taux_tva = taux_tva;
		this.montant = montant;

	}

	public Facture() {super();}

	public int getIdfacture() {return idfacture;}
	public void setIdfacture(int idfacture) {this.idfacture = idfacture;}

	public String getClient() {return client;}
	public void setClient(String client) {this.client = client;}
	
	public String getNum_facture() {return num_facture;}
	public void setNum_facture(String num_facture) {this.num_facture = num_facture;}

	public String getDate_facture() {return date_facture;}
	public void setDate_facture(String date_facture) {this.date_facture = date_facture;}

	public String getMessage() {return message;}
	public void setMessage(String message) {this.message = message;}

	public String getNum_engagement() {return num_engagement;}
	public void setNum_engagement(String num_engagement) {this.num_engagement = num_engagement;}
	
	public String getCode_service() {return code_service;}
	public void setCode_service(String code_service) {this.code_service = code_service;}
	
	public String getCondition_paiement() { return condition_paiement; }
	public void setCondition_paiement(String condition_paiement) { this.condition_paiement = condition_paiement; }
	
	public String getEcheance() { return echeance; }
	public void setEcheance(String echeance) { this.echeance = echeance; }
	
	public String getConsultant() { return consultant; }
	public void setConsultant(String consultant) { this.consultant = consultant; }
	
	public String getAnnee() {return annee;}
	public void setAnnee(String annee) {this.annee = annee;}

	public String getMois() {return mois;}
	public void setMois(String mois) {this.mois = mois;}

	public String getNbr_jours() {return nbr_jours;}
	public void setNbr_jours(String nbr_jours) {this.nbr_jours = nbr_jours;}

	public String getUnite() {return unite;}
	public void setUnite(String unite) {this.unite = unite;}

	public String getTjm() {return tjm;}
	public void setTjm(String tjm) {this.tjm = tjm;}
	
	public String getTaux_tva() {return taux_tva;}
	public void setTaux_tva(String taux_tva) {this.taux_tva = taux_tva;}
	
	public String getMontant() {return montant;}
	public void setMontant(String montant) {this.montant = montant;}

	@Override
	public String toString() {
		return "Client [idfacture=" + idfacture + ", client=" + client + ", num_facture=" + num_facture
				+ ", date_facture=" + date_facture + ", message=" + message + ", num_engagement=" + num_engagement 
				+ ", code_service=" + code_service + ","+ " condition_paiement=" + condition_paiement + ", echeance=" + echeance
				+ ", consultant=" + consultant + ", annee=" + annee + ", mois=" + mois + ", nbr_jours=" + nbr_jours 
				+ ", unite=" + unite + ", tjm=" + tjm + ", taux_tva=" + taux_tva + ", montant=" + montant + "]";
	}






}
