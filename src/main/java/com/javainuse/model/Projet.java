package com.javainuse.model;



import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="projet")
public class Projet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idprojet;
	
	@Column(name = "libelle")
	private String libelle;
	
	@Column(name = "date_lancement")
	private String datelancement;
	
	@Column(name = "techno")
	private String techno;
	
	@Column(name = "client")
	private String client;
	
	@Column(name = "status")
	private String status;
	
	public Projet(int idprojet, String libelle, String datelancement,String client,String techno,String status) {
		super();
		this.idprojet = idprojet;
		this.libelle = libelle;	
		this.datelancement=datelancement;
		this.client=client;
		this.techno=techno;
		this.status = status;
	}

	public Projet() {
		super();
	}

	public int getIdprojet() {
		return idprojet;
	}

	public void setIdprojet(int idprojet) {
		this.idprojet = idprojet;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public String getDatelancement() {
		return datelancement;
	}

	public void setDatelancement(String datelancement) {
		this.datelancement = datelancement;
	}

	public String getTechno() {
		return techno;
	}

	public void setTechno(String techno) {
		this.techno = techno;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Projet [idprojet=" + idprojet + ", libelle=" + libelle + ", datelancement=" + datelancement
				+ ", techno=" + techno + ", client=" + client + ", status=" + status + "]";
	}






}
