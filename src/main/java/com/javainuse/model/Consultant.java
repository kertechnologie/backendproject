
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="consultant")
public class Consultant implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idconsultant;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "ref")
	private String ref;
		
	@Column(name = "competence")
	private String competence;
	
	@Column(name = "specialite")
	private String specialite;
	
	@Column(name = "statut")
	private String statut;
	
	@Column(name = "client")
	private String client;
	
	@Column(name = "tjm")
	private String tjm;
	
	public Consultant(int idconsultant, String nom, String ref, String competence, String specialite, String statut, String client, String tjm) {
		super();
		this.idconsultant = idconsultant;
		this.nom = nom;
		this.ref = ref;
		this.competence = competence;
		this.specialite = specialite;	
		this.statut = statut;
		this.client = client;
		this.tjm = tjm;
	}

	public Consultant() {
		super();
	}

	public int getIdconsultant() { return idconsultant; }

	public void setIdconsultant(int idconsultant) { this.idconsultant = idconsultant; }
	
	public String getNom() { return nom; }

	public void setNom(String nom) { this.nom = nom; }
	
	public String getRef() { return ref; }

	public void setRef(String ref) { this.ref = ref; }

	public String getCompetence() { return competence; }

	public void setCompetence(String competence) { this.competence = competence; }
	
	public String getSpecialite() { return specialite; }

	public void setSpecialite(String specialite) { this.specialite = specialite; }
	
	public String getStatut() { return statut; }

	public void setStatut(String statut) { this.statut = statut; }
	
	public String getClient() { return client; }

	public void setClient(String client) { this.client = client; }
	
	public String getTjm() { return tjm; }

	public void setTjm(String tjm) { this.tjm = tjm; }

	@Override
	public String toString() {
		return "Candidat [idconsultant=" + idconsultant + ", nom=" + nom + ", ref=" + ref + ", competence=" + competence + ", specialite=" + specialite + ", statut=" + statut + ", client=" + client + ", tjm=" + tjm + "]";
	}






}
