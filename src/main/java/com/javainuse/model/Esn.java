
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="esn")
public class Esn implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idesn;
	
	@Column(name = "societe")
	private String societe;
	
	@Column(name = "interlocuteur")
	private String interlocuteur;
	
	@Column(name = "poste")
	private String poste;
	
	@Column(name = "tel_fixe")
	private String tel_fixe;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "commentaire")
	private String commentaire;
	
	public Esn(int idesn, String societe, String interlocuteur,String poste,String tel_fixe, String mobile, String email, String commentaire) {
		super();
		this.idesn = idesn;
		this.societe = societe;	
		this.interlocuteur = interlocuteur;
		this.poste = poste;
		this.tel_fixe = tel_fixe;
		this.mobile = mobile;
		this.email = email;
		this.commentaire = commentaire;
	}

	public Esn() {
		super();
	}

	public int getIdesn() {
		return idesn;
	}

	public void setIdesn(int idesn) {
		this.idesn = idesn;
	}

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}
	
	public String getInterlocuteur() {
		return interlocuteur;
	}

	public void setInterlocuteur(String interlocuteur) {
		this.interlocuteur = interlocuteur;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getTel_fixe() { return tel_fixe; }

	public void setTel_fixe(String tel_fixe) { this.tel_fixe = tel_fixe; }
	
	public String getMobile() { return mobile; }

	public void setMobile(String mobile) { this.mobile = mobile; }
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	public String toString() {
		return "Client [idesn=" + idesn + ", societe=" + societe + ", interlocuteur=" + interlocuteur
				+ ", poste=" + poste + ", tel_fixe=" + tel_fixe + ", mobile=" + mobile + ", email=" + email +  ", commentaire=" + commentaire + "]";
	}


}
