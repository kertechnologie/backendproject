
package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Esn;

public interface EsnRepository extends CrudRepository<Esn,Integer> {
	
	Esn save(Esn esn);
	List<Esn> findAll();
	void deleteById(int id); 
	Esn findById(int id);
}
