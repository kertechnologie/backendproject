
package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Facture;

@Repository
public interface FactureRepository extends CrudRepository<Facture,Integer> {

	Facture save(Facture facture);
	List<Facture> findAll();
	void deleteById(int id); 
	Facture findById(int id);
}
