
package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client,Integer> {

	Client save(Client client);
	List<Client> findAll();
	void deleteById(int id); 
	Client findById(int id);
}
