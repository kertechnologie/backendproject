package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.javainuse.model.Conges;
import com.javainuse.model.Projet;
import com.javainuse.model.User;

public interface CongesRepository  extends CrudRepository<Conges,Integer>{
	
	Conges save(Conges conges);
	List<Conges> findAll();
	void deleteById(int id); 
	Conges findById(int id);
	List <Conges> findByUser (User user);

}
