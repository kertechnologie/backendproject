
package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Consultant;

public interface ConsultantRepository extends CrudRepository<Consultant,Integer> {
	
	Consultant save(Consultant consultant);
	List<Consultant> findAll();
	void deleteById(int id); 
	Consultant findById(int id);
}

