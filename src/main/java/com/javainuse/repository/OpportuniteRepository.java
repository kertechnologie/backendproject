package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Opportunite;

public interface OpportuniteRepository extends CrudRepository<Opportunite,Integer> {
	
	Opportunite save(Opportunite opportunite);
	List<Opportunite> findAll();
	void deleteById(int id); 
	Opportunite findById(int id);
}