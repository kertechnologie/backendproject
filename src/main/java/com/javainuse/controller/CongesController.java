package com.javainuse.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.model.Conges;
import com.javainuse.model.User;
import com.javainuse.repository.CongesRepository;
import com.javainuse.repository.UserRepository;
import com.javainuse.SmtpMailSender;

@RestController
@CrossOrigin(origins = "*")
public class CongesController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String Idemp = null;
	@Autowired
	CongesRepository congesRepoistory;
	@Autowired
	private SmtpMailSender smtpMailSender;
	@Autowired
	UserRepository userRepository;
	
	@PostMapping("/addConges")
	public Conges add(@RequestBody Conges conges) throws NoSuchAlgorithmException, MessagingException {
	
		 int iduser = conges.getIdconge();
		User user = userRepository.findById(conges.getUser().getIdemp());
		String first = user.getPrenom();
		String last = user.getNom();
		
		smtpMailSender.sendingMail("contact@kertechnologie.fr", "Demande de congé","Bonjour,\r\n" + 
				"\r\n" +  first + " " + last  + " demande un congé" + " de" + " " + conges.getDate_debut() + " jusqu'à" + " " + conges.getDate_fin()+
				".\r\n" + 
				"\r\n" + 
				"Cordialement,\r\n" + 
				"");	
		return congesRepoistory.save(conges);	
	}

	@GetMapping("/AllConges")
	public List<Conges> allProjects() {
		return congesRepoistory.findAll();
	}

	@GetMapping("/conges/{id}")
	public Conges show(@PathVariable String id) throws MessagingException {
		int idconges = Integer.parseInt(id);
		
		Conges cong = congesRepoistory.findById(idconges);
		User user = userRepository.findById(cong.getUser().getIdemp());
		//String mail = user.getEmail();
		
		
		
		return congesRepoistory.findById(idconges);
	}
	
	@GetMapping("/Userconges/{id}")
	public List<Conges> Userconges(@PathVariable String id) throws MessagingException {
		int iduser = Integer.parseInt(id);
		//int idcong = Integer.parseInt(ide);
		User user = userRepository.findById(iduser);
		/* smtpMailSender.sendingMail(user.getEmail(), "Réponse de la demande du congé","Bonjour,\r\n" + 
				"\r\n" + 
				"Suite à votre demande de congé, veuillez consulter votre espace personnel.\r\n" + 
				"\r\n" + 
				"Cordialement,**Ceci est un email automatique, merci de ne pas répondre**\r\n" + 
				"");	*/
		return congesRepoistory.findByUser(user);
	}

	@GetMapping("/deleteconges/{id}")
	public void delete(@PathVariable String id) {
		int idconges = Integer.parseInt(id);
		congesRepoistory.deleteById(idconges);
	}

	@PostMapping("/updateconges")
	public ResponseEntity<?> update(@RequestBody Conges congess) throws NoSuchAlgorithmException, MessagingException {
		User user = userRepository.findById(congess.getUser().getIdemp());
		String first = user.getPrenom();
		String last = user.getNom();
		String rol = user.getRole();
		String maile = user.getEmail();
		
		if ( congess.getIdconge() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			 if(congess.getEtat_demande().equals("En cours")) {
				smtpMailSender.sendingMail("contact@kertechnologie.fr", "Modification de la date de congé","Bonjour,\r\n" + 
						"\r\n" +  first + " " + last  + " a modifié son congé" + " :" + " " + congess.getDate_debut() + " jusqu'à" + " " + congess.getDate_fin()+
						".\r\n" + 
						"\r\n" + 
						"Cordialement,\r\n" + 
						"");
			 }
			 else {
				 smtpMailSender.sendingMail(maile, "Réponse de la demande du congé","Bonjour,\r\n" + 
							"\r\n" + 
							"Suite à votre demande de congé, veuillez consulter votre espace personnel.\r\n" + 
							"\r\n" + 
							"Cordialement,   **Ceci est un email automatique, merci de ne pas répondre**\r\n" + 
							"");
			 }
			return ResponseEntity.ok(congesRepoistory.save(congess));
		}

	}
}

