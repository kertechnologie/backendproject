
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Consultant;
import com.javainuse.repository.ConsultantRepository;

@RestController
@CrossOrigin(origins = "*")
public class ConsultantController {

	@Autowired
	ConsultantRepository consultantRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aConsultant")
	public Consultant add(@RequestBody Consultant consultant) throws NoSuchAlgorithmException {

		return consultantRepository.save(consultant);

	}

	@GetMapping("/AllConsultants")
	public List<Consultant> AllConsultants() {
		return consultantRepository.findAll();
	}

	@GetMapping("/consultant/{id}")
	public Consultant show(@PathVariable String id) {
		int idconsultant = Integer.parseInt(id);
		return consultantRepository.findById(idconsultant);
	}

	@GetMapping("/deleteConsultant/{id}")
	public void delete(@PathVariable String id) {
		int idconsultant = Integer.parseInt(id);
		consultantRepository.deleteById(idconsultant);
	}

	@PostMapping("/updateConsultant")
	public ResponseEntity<?> update(@RequestBody Consultant consultant) throws NoSuchAlgorithmException {

		if (consultant.getIdconsultant() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}
		else {
			return ResponseEntity.ok(consultantRepository.save(consultant));
		}

	}
}
