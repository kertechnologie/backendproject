
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Prospect;
import com.javainuse.repository.ProspectRepository;

@RestController
@CrossOrigin(origins = "*")
public class ProspectController {

	@Autowired
	ProspectRepository prospectRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aProspect")
	public Prospect add(@RequestBody Prospect prospect) throws NoSuchAlgorithmException {

		return prospectRepository.save(prospect);

	}

	@GetMapping("/AllProspects")
	public List<Prospect> AllProspects() {
		return prospectRepository.findAll();
	}

	@GetMapping("/prospect/{id}")
	public Prospect show(@PathVariable String id) {
		int idprospect = Integer.parseInt(id);
		return prospectRepository.findById(idprospect);
	}

	@GetMapping("/deleteProspect/{id}")
	public void delete(@PathVariable String id) {
		int idprospect = Integer.parseInt(id);		
		prospectRepository.deleteById(idprospect);
	}

	@PostMapping("/updateProspect")
	public ResponseEntity<?> update(@RequestBody Prospect prospect) throws NoSuchAlgorithmException {

		if (prospect.getIdprospect() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(prospectRepository.save(prospect));
		}

	}
}
