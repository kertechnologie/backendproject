
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Opportunite;
import com.javainuse.repository.OpportuniteRepository;

@RestController
@CrossOrigin(origins = "*")
public class OpportuniteController {

	@Autowired
	OpportuniteRepository opportuniteRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aOpportunite")
	public Opportunite add(@RequestBody Opportunite opportunite) throws NoSuchAlgorithmException {

		return opportuniteRepository.save(opportunite);

	}

	@GetMapping("/AllOpportunites")
	public List<Opportunite> AllOpportunites() {
		return opportuniteRepository.findAll();
	}

	@GetMapping("/Opportunite/{id}")
	public Opportunite show(@PathVariable String id) {
		int idopportunite = Integer.parseInt(id);
		return opportuniteRepository.findById(idopportunite);
	}

	@GetMapping("/deleteOpportunite/{id}")
	public void delete(@PathVariable String id) {
		int idopportunite = Integer.parseInt(id);		
		opportuniteRepository.deleteById(idopportunite);
	}

	@PostMapping("/updateOpportunite")
	public ResponseEntity<?> update(@RequestBody Opportunite opportunite) throws NoSuchAlgorithmException {

		if (opportunite.getIdopportunite() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(opportuniteRepository.save(opportunite));
		}

	}
}

