
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Candidat;
import com.javainuse.repository.CandidatRepository;

@RestController
@CrossOrigin(origins = "*")
public class CandidatController {

	@Autowired
	CandidatRepository candidatRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aCandidat")
	public Candidat add(@RequestBody Candidat candidat) throws NoSuchAlgorithmException {

		return candidatRepository.save(candidat);

	}

	@GetMapping("/AllCandidats")
	public List<Candidat> AllCandidats() {
		return candidatRepository.findAll();
	}

	@GetMapping("/candidat/{id}")
	public Candidat show(@PathVariable String id) {
		int idcandidat = Integer.parseInt(id);
		return candidatRepository.findById(idcandidat);
	}

	@GetMapping("/deleteCandidat/{id}")
	public void delete(@PathVariable String id) {
		int idcandidat = Integer.parseInt(id);
		candidatRepository.deleteById(idcandidat);
	}

	@PostMapping("/updateCandidat")
	public ResponseEntity<?> update(@RequestBody Candidat candidat) throws NoSuchAlgorithmException {

		if (candidat.getIdcandidat() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(candidatRepository.save(candidat));
		}

	}
}
