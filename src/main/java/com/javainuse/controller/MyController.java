package com.javainuse.controller;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.config.JwtTokenUtil;
import com.javainuse.model.ChangePasswordForm;
import com.javainuse.model.JwtResponse;
import com.javainuse.model.LoginForm;
import com.javainuse.model.User;
import com.javainuse.repository.AffectationRepository;
import com.javainuse.repository.UserRepository;

@RestController
@CrossOrigin(origins = "*")
public class MyController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	UserRepository UserRepository;

	@Autowired
	AffectationRepository affectationRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@GetMapping("/user")
	public List<User> index() {
		return UserRepository.findAll();
	}
	

	@GetMapping("/user/{id}")
	public User show(@PathVariable String id) {
		int idemp = Integer.parseInt(id);
		return UserRepository.findById(idemp);
	}

	@PostMapping("/register")
	public ResponseEntity<?> save(@RequestBody User user) throws NoSuchAlgorithmException {

		if (UserRepository.findByPseudo(user.getPseudo()) != null) {

			return ResponseEntity.badRequest().body("user already exists !");
		}
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		String ss = Base64.getEncoder().encodeToString(salt);
		byte[] hashedPassword = md.digest((ss + user.getPwd()).getBytes(StandardCharsets.UTF_8));
		System.out.println(hashedPassword);
		user.setPwd(Base64.getEncoder().encodeToString(hashedPassword));
		user.setSalt(Base64.getEncoder().encodeToString(salt));
		System.out.println(user.getAffectations());
		return ResponseEntity.ok(UserRepository.save(user));
		// return ResponseEntity.ok("test");
	}
	
	@PostMapping("/updatePassword")
	public ResponseEntity<?> updateUserPassword(@RequestBody ChangePasswordForm changePasswordForm) throws NoSuchAlgorithmException {

		if (changePasswordForm.getPseudo() == null || 
				changePasswordForm.getOldPwd() == null  || 
				changePasswordForm.getNewPwd() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST);
			// TokenResponse tokenResponse = null;
		}

		User userTemp = UserRepository.findByPseudo(changePasswordForm.getPseudo());

		if (userTemp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
			// TokenResponse tokenResponse = null;
		}

		String salt = userTemp.getSalt();
		String condidate = changePasswordForm.getOldPwd();

		MessageDigest md = MessageDigest.getInstance("SHA-512");
		String hashed = Base64.getEncoder()
				.encodeToString(md.digest((salt + condidate).getBytes(StandardCharsets.UTF_8)));

		if (hashed.equals(userTemp.getPwd())) {
			
			SecureRandom random = new SecureRandom();
			byte[] newSalt = new byte[16];
			random.nextBytes(newSalt);
			String ss = Base64.getEncoder().encodeToString(newSalt);
			byte[] hashedPassword = md.digest((ss + changePasswordForm.getNewPwd()).getBytes(StandardCharsets.UTF_8));
			
			userTemp.setPwd(Base64.getEncoder().encodeToString(hashedPassword));
			userTemp.setSalt(Base64.getEncoder().encodeToString(newSalt));
			
			UserRepository.save(userTemp);
			final String token = jwtTokenUtil.generateTokenUser(userTemp);
			return ResponseEntity.ok(new JwtResponse(token));

		} else {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN, HttpStatus.FORBIDDEN);

		}

	}

	@PostMapping("/update")
	public ResponseEntity<?> update(@RequestBody User user) {
		

		User usertemp = UserRepository.findById(user.getIdemp());
		if (usertemp == null) {
			return ResponseEntity.badRequest().body("user does not exist !");
		}
		else {
			user.setAffectations(usertemp.getAffectations());
			return ResponseEntity.ok(UserRepository.save(user));
		}
	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginForm loginForm) throws NoSuchAlgorithmException {

		if (loginForm.getPseudo() == null || loginForm.getPwd() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST);
			// TokenResponse tokenResponse = null;
		}

		User userTemp = UserRepository.findByPseudo(loginForm.getPseudo());

		if (userTemp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
			// TokenResponse tokenResponse = null;
		}

		String salt = userTemp.getSalt();
		String condidate = loginForm.getPwd();

		MessageDigest md = MessageDigest.getInstance("SHA-512");
		String hashed = Base64.getEncoder()
				.encodeToString(md.digest((salt + condidate).getBytes(StandardCharsets.UTF_8)));

		if (hashed.equals(userTemp.getPwd())) {
			final String token = jwtTokenUtil.generateTokenUser(userTemp);
			return ResponseEntity.ok(new JwtResponse(token));

		} else {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN, HttpStatus.FORBIDDEN);

		}
	}

	@GetMapping("/delete")
	public void deleteAllUser() {
		UserRepository.deleteAll();
	}

	@GetMapping("/delete/{id}")
	public void delete(@PathVariable String id) {
		int idemp = Integer.parseInt(id);
		System.out.println(idemp);
		UserRepository.deleteById(idemp);
	}



}