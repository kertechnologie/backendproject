
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Esn;
import com.javainuse.repository.EsnRepository;

@RestController
@CrossOrigin(origins = "*")
public class EsnController {

	@Autowired
	EsnRepository esnRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aEsn")
	public Esn add(@RequestBody Esn esn) throws NoSuchAlgorithmException {

		return esnRepository.save(esn);

	}

	@GetMapping("/AllEsns")
	public List<Esn> AllEsns() {
		return esnRepository.findAll();
	}

	@GetMapping("/esn/{id}")
	public Esn show(@PathVariable String id) {
		int idesn = Integer.parseInt(id);
		return esnRepository.findById(idesn);
	}

	@GetMapping("/deleteEsn/{id}")
	public void delete(@PathVariable String id) {
		int idesn = Integer.parseInt(id);		
		esnRepository.deleteById(idesn);
	}

	@PostMapping("/updateEsn")
	public ResponseEntity<?> update(@RequestBody Esn esn) throws NoSuchAlgorithmException {

		if (esn.getIdesn() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(esnRepository.save(esn));
		}

	}
}
