package com.javainuse.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.model.Affectation;
import com.javainuse.model.Tache;
import com.javainuse.model.User;
import com.javainuse.repository.TacheRepository;
import com.javainuse.repository.UserRepository;
import com.javainuse.repository.AffectationRepository;


@RestController
@CrossOrigin(origins = "*")

public class TacheController<S> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	TacheRepository tacheRepository;
	@Autowired
	AffectationRepository affectationRepository;
	@Autowired
	UserRepository userRepoistory;
	

	@PostMapping("/SaveTache")
	public  String save(@RequestBody List<Tache> taches) {

	    if (taches == null) {
	        return "null";
	    }
	    
	    int taches_length = taches.size();
	    for (int i=0;i<taches_length;i++)
	    {
	    	Tache tache= taches.get(i);
	    	tacheRepository.save(tache);
	    }
	  //  JavaMailUtil.senMail(recepiant);
		return "List Added";
 
	}
	
	 @GetMapping("/AllTache")
	 	public List<Tache> allTaks(){
	        return  tacheRepository.findAll();
	    }
	 
	 @GetMapping("/Tache/{id}")
	 	public List<Tache> TasksByUser(@PathVariable String id) throws Exception{
			int idemp = Integer.parseInt(id);
			User user= userRepoistory.findById(idemp);
			List <Affectation> affectation = affectationRepository.findByUser(user);
			List<Tache> tache = new ArrayList<>();
			for (int i=0;i<=affectation.size()-1;i++) {
				System.out.println(affectation.get(i).getIdaffectation()+'\n');
				int affectationId = affectation.get(i).getIdaffectation();
				Affectation affectations=affectationRepository.findById(affectationId);
				tache.addAll(tacheRepository.findByAffectation(affectations));
				 //tache.(tacheRepository.findByAffectation(affectationId));
			}
			//tache.add(tasks);
			
			return tache;
			
	    }
}