
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Facture;
import com.javainuse.repository.FactureRepository;

@RestController
@CrossOrigin(origins = "*")
public class FactureController {

	@Autowired
	FactureRepository factureRepository;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aFacture")
	public Facture add(@RequestBody Facture facture) throws NoSuchAlgorithmException {

		return factureRepository.save(facture);

	}

	@GetMapping("/AllFactures")
	public List<Facture> AllFactures() {
		return factureRepository.findAll();
	}

	@GetMapping("/facture/{id}")
	public Facture show(@PathVariable String id) {
		int idfacture = Integer.parseInt(id);
		return factureRepository.findById(idfacture);
	}

	@GetMapping("/deleteFacture/{id}")
	public void delete(@PathVariable String id) {
		int idfacture = Integer.parseInt(id);		
		factureRepository.deleteById(idfacture);
	}

	@PostMapping("/updateFacture")
	public ResponseEntity<?> update(@RequestBody Facture facture) throws NoSuchAlgorithmException {

		if (facture.getIdfacture() == 0) {
			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(factureRepository.save(facture));
		}

	}
}
