
package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Client;
import com.javainuse.repository.ClientRepository;

@RestController
@CrossOrigin(origins = "*")
public class ClientController {

	@Autowired
	ClientRepository clientRepoistory;
	
	// @Autowired AffectationRepository affectationRepository;

	@PostMapping("/aClient")
	public Client add(@RequestBody Client client) throws NoSuchAlgorithmException {

		return clientRepoistory.save(client);

	}

	@GetMapping("/AllClients")
	public List<Client> AllClients() {
		return clientRepoistory.findAll();
	}

	@GetMapping("/client/{id}")
	public Client show(@PathVariable String id) {
		int idclient = Integer.parseInt(id);
		return clientRepoistory.findById(idclient);
	}

	@GetMapping("/deleteClient/{id}")
	public void delete(@PathVariable String id) {
		int idclient = Integer.parseInt(id);
	   // Client clientTemp = clientRepoistory.findById(idclient);
		//affectationRepository.deleteByClient(clientTemp);
		
		clientRepoistory.deleteById(idclient);
	}

	@PostMapping("/updateClient")
	public ResponseEntity<?> update(@RequestBody Client client) throws NoSuchAlgorithmException {

		if (client.getIdclient() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(clientRepoistory.save(client));
		}

	}
}
