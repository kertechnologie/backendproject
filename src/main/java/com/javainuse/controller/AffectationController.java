package com.javainuse.controller;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Affectation;
import com.javainuse.model.User;
import com.javainuse.repository.AffectationRepository;
import com.javainuse.repository.ProjetRepository;
import com.javainuse.repository.UserRepository;

@RestController
@CrossOrigin(origins = "*")

public class AffectationController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	AffectationRepository affectationRepository;
	@Autowired
	UserRepository userRepoistory;
	@Autowired 
	ProjetRepository projetRepository;
	

	
	
	@PostMapping("/SaveAffectation")
	public ResponseEntity<?> update(@RequestBody List<Affectation> affectations) {
		
		User user = userRepoistory.findById(affectations.get(0).getUser().getIdemp());
		affectationRepository.deleteByUser(user);
		
		if(affectations.get(0).getProjet() != null) {
		   	 user.setAffectations(affectations);
			 userRepoistory.save(user);
			
		}
		
		return ResponseEntity.ok(user.getAffectations());
	
	}
	
	
	 @GetMapping("/AllAffectation")
	 	public List<Affectation> allProjects(){
	        return  affectationRepository.findAll();
	    }
	@GetMapping("/getAffectationByUser/{id}")
	public List<Affectation> getAffectationByUser(@PathVariable String id){
		int idemp = Integer.parseInt(id);
		User user= userRepoistory.findById(idemp);
		return affectationRepository.findByUser(user);
	}
}