package com.javainuse;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;

import org.springframework.web.filter.CharacterEncodingFilter;

public class sendMailing {
	 
	public static void senMail(String recepiant) throws Exception {
		//System.out.println("preparer pour envoyer un mail");
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "send.one.com");
		properties.put("mail.smtp.port", "587");
		String MyAccountEmail = "noreply@kertechnologie.fr";
		String Password ="Kertechnologie2020";
		Session session = Session.getDefaultInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(MyAccountEmail, Password);
			}
		});
				
		Message message = prepareMessage(session, MyAccountEmail, recepiant);
		
		Transport.send(message);
		System.out.println("message est envoyé");
			
	}

	private static Message prepareMessage(Session session,String MyAccountEmail,String recepiant) throws Exception {	
		String s1 = "Bonjour,<br/><br/>\r\n"
				+ "\r\n"
				+ "Merci de valider votre CRA en ligne et envoyer une copie à votre commerciale.<br/><br/>\r\n"
				+ "\r\n"
				+ "Cordialement,<br/>\r\n"
				+ "\r\n"
				+ "**Ceci est un email automatique, merci de ne pas répondre**";
	 //	byte[] bytes = s1.getBytes("UTF-8"); // Charset to encode into
		//String s2 = new String(bytes, "UTF-8"); // Charset with which bytes were encoded 

			try {
				Message message = new MimeMessage(session);
			    message.setHeader("Content-Type", "text/plain; charset=UTF-8");
				message.setFrom(new InternetAddress(MyAccountEmail));
				message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepiant));
				message.setSubject("Validation du CRA");
				message.setContent(s1, "text/html; charset=utf-8");
				return message;
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		return null;
	}

}


